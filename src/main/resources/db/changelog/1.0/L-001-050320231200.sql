CREATE SCHEMA lamps_surf2023;

CREATE TABLE lamps_surf2023.lamps(
    id          SERIAL PRIMARY KEY ,
    brand       VARCHAR(25),
    model       VARCHAR(50),
    power_l     FLOAT,
    matt        BOOLEAN,
    dim         BOOLEAN,
    color_l     INTEGER,
    lm_l        INTEGER,
    eq_l        INTEGER,
    ra_l        INTEGER,
    u           VARCHAR(10),
    pf_l        FLOAT,
    angle_l     VARCHAR,
    life        INTEGER,
    war         INTEGER,
    prod        INTEGER,
    w           INTEGER,
    d           INTEGER,
    h           INTEGER,
    t           INTEGER,
    barcode     INTEGER,
    plant       VARCHAR(20),
    base        VARCHAR(5),
    shape       VARCHAR(15),
    type        VARCHAR(10),
    type2       VARCHAR(10),
    url         VARCHAR(100),
    shop        VARCHAR(50),
    rub         FLOAT,
    usd         FLOAT,
    p           FLOAT,
    pf          INTEGER,
    lm          INTEGER,
    color       INTEGER,
    cri         FLOAT,
    r9          INTEGER,
    Rf          INTEGER,
    Rg          INTEGER,
    Duv         INTEGER,
    flicker     INTEGER,
    angle       INTEGER,
    switch      BOOLEAN,
    umin        INTEGER,
    drv         INTEGER,
    tmax        INTEGER,
    date        TIMESTAMP,
    instruments VARCHAR(15),
    add2        VARCHAR(25),
    add3        VARCHAR(25),
    add4        VARCHAR(25),
    add5        VARCHAR(25),
    cqs         INTEGER,
    eq          INTEGER,
    rating      TIMESTAMP,
    act         BOOLEAN,
    lamp_image  VARCHAR(100),
    lamp_desc   VARCHAR(100)
);