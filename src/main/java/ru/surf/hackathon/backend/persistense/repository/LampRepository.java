package ru.surf.hackathon.backend.persistense.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.surf.hackathon.backend.persistense.entity.Lamp;

import java.util.UUID;

@Repository
public interface LampRepository extends JpaRepository<Lamp, UUID> {
    Lamp findBybarcode(Integer barcode);
}