package ru.surf.hackathon.backend.persistense.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = "lamps", schema = "lamps_surf2023")
public class Lamp {
    @Id
    @GeneratedValue
    private Integer id;
    private String brand;
    private Float power_l;
    private Boolean dim;
    private Integer lm_l;
    private Integer ra_l;
    private Integer eq_l ;
    private String u ;
    private Float pf_l;
    private String angle_l;
    private Integer life;
    private Integer war;
    private Integer prod;
    private Integer w;
    private Integer d ;
    private Integer h ;
    private Integer t;
    private String barcode;
    private String plant;
    private String base;
    private String shape;
    private String type ;
    private String type2;
    private String url;
    private String shop;
    private Float rub ;
    private Float usd;
    private Float p;
    private Integer pf;
    private Integer lm;
    private Integer color ;
    private Float cri;
    private Integer r9;
    private Integer Rf;
    private Integer Rg;
    private Integer Duv;
    private Integer flicker;
    private Integer angle;
    @Column(name="switch")
    private Boolean switchh;
    private Integer umin ;
    private Integer drv;
    private Integer tmax ;
    private Date date ;
    private String instruments;
    private String add2;
    private String add3;
    private String add4;
    private String add5;
    private Integer cqs;
    private Integer eq;
    private Boolean act;
    private String lamp_image;
    private String lamp_desc;



}
