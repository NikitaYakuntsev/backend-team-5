package ru.surf.hackathon.backend.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.surf.hackathon.backend.service.LampService;
import ru.surf.hackathon.backend.service.model.LampDto;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/lamps")
@CrossOrigin
public class LampController {
    private final LampService lampService;

    @Autowired
    public LampController(LampService lampService) {
        this.lampService = lampService;
    }

    @GetMapping(value = "/barcode/{barcode}")

    LampDto findByBarcode(@PathVariable(name = "barcode") Integer barcode) {
        return lampService.findByBarcode(barcode);
    }

    @GetMapping("/barcodes")
    List<LampDto> findByBarcodes(@RequestBody List<Integer> barcodes){
        return lampService.findByBarcodes(barcodes);
    }
}