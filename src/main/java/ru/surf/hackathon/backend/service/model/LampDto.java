package ru.surf.hackathon.backend.service.model;
public class LampDto {
    private Integer id;

    public LampDto(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
