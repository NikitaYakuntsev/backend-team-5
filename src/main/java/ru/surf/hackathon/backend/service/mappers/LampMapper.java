package ru.surf.hackathon.backend.service.mappers;

import org.mapstruct.Mapper;
import ru.surf.hackathon.backend.persistense.entity.Lamp;
import ru.surf.hackathon.backend.service.model.LampDto;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LampMapper {
    LampDto fromEntity(Lamp entity);

    Lamp toEntity(LampDto dto);
    List<LampDto> fromEntities(Iterable<Lamp> entites);
}
