package ru.surf.hackathon.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.surf.hackathon.backend.controller.exception.ApiRequestException;
import ru.surf.hackathon.backend.persistense.entity.Lamp;
import ru.surf.hackathon.backend.persistense.repository.LampRepository;
import ru.surf.hackathon.backend.service.mappers.LampMapper;
import ru.surf.hackathon.backend.service.model.LampDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class LampService {
    private final LampRepository repository;
    private final LampMapper mapper;

    @Autowired
    public LampService(LampRepository repository, LampMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public LampDto findByBarcode(Integer barcode) {
        return Optional.of(repository.findBybarcode(barcode))
                .map(mapper::fromEntity)
                .orElseThrow(() -> new ApiRequestException("Bad barcode"));
    }

    public List<LampDto> findByBarcodes(List<Integer> barcodes){
        List<Lamp> entities = new ArrayList<>();

        for(Integer s : barcodes){
            entities.add(repository.findBybarcode(s));
        }

        return Optional.of(mapper.fromEntities(entities)).orElseThrow();
    }
}
